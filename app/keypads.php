<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class keypads extends Model
{
    protected $table="user_keypads";
	
	
	protected $fillable = [
        'id','user_id','keypad_id','door_name','email','phone_number','status',
    ];
}
