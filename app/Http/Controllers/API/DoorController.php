<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\UserSession;
use App\keypads;
use App\UserKeypad;
use App\Doors;
use App\Traits\Common;

class DoorController extends Controller
{ 
	use Common;
	
	public function lock_unlock(Request $request)
	{
		try
		{
			if(!empty($request->user_id)&&!empty($request->door_id)&&$request->status!="")
			{
				$users = User::findOrFail($request->user_id);
				if($users)
				{
					$door=Doors::find($request->door_id);
					if($door)
					{
						$access=Userkeypad::where('user_id',$request->user_id)->where('keypad_id',$request->door_id)->first();
						if($access)
						{
							if($request->status==0)
							{
								$message="Trigger unlock";
								$this->send_sms($door->number,$message);
							}
							elseif($request->status==1)
							{
								$message="Trigger lock";
								$this->send_sms($door->number,$message);
							}
							else
							{
								return response()->json(['status'=>400,'message'=>'invalid status']);
							}
						}
						else
						{
							return response()->json(['status'=>400,'message'=>'invalid user']);
						}
					}
					else
					{
						return response()->json(['status'=>400,'message'=>'door not found']);
					}
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'user not found']);
				}
			}
			else 
			{
				return response()->json(['status'=>400,'message'=>'param missing']);
			}
		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);	
		}
				 
	}

	public function reciever(Request $request)
	{
		try
		{
			// if(!empty($request->user_id)&&!empty($request->door_id)&&$request->status!="")
			// {

$token = "_V-KnG6h8wtY6PK7MTyOyoOvyyRWi2TI6qLFo9AZgjA"; // TODO a dummy token for now. it should be generated from the password sent by the lock.
  
         // read the POST data
  // $input = json_decode($request->all(), true);               // get an array object
$input=$request;

  $password = $input['password'];                    	 // extract the password from the array
  $keypad = $input['keypad']; 
                        	 // extract the keypad ID from the array
  $locked = $input['locked'];				// true or false
  $bell = $input['bell'];					// true if bell has been pushed
  $error = $input['error'];					//-1 -> unknown,  0 -> no error, 1 -> invalid request, 2 -> invalid password, 3 -> not implemented yet, others TBD
  $open = $input['open'];					// 0 -> closed
 
// TODO validate these parameters, e.g. check that the password is correct and handle any errors.
// TODO create a new token from the received password. The lock will check that the decrypted token matches the password.
// NOTE the encryption method and public/private keys are TBD. Until then the lock will accept any token.

// Store the latest status
$current_status = json_encode
  (array
	(
	"keypad" => $keypad,
	"token" => $token,
	"locked" => $locked, 
	"bell" => $bell,
	"error" => $error,
	"open" => $open
	)
  );    // convert to json

// Get the current lock/unlock request TODO This should extract the required message from a database for this lock ID.
  $update=Doors::where('id',$keypad)->update(['status'=>$locked,'bell_request'=>$bell]);
  $lock_request=true;
  if($update)
  {
  	$reply_json = json_encode(array("keypad" => $keypad,"token" => $token,"lock_request" => $lock_request));    // convert to json
  	echo ($reply_json);
  }
  else
  {
  	$reply_json = json_encode(array("keypad" => $keypad,"error" => 1));    // convert to json
  echo ($reply_json); 
  }


		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);	
		}
				 
	}


}
