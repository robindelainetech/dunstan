<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Hash;
use DB;
use App\User;
use App\UserSession;
use App\keypads;
use App\Userkeypad;
use App\Doors;
use App\Invites;
use App\Traits\Common;

class UserController extends Controller
{ 
	use Common;
	
	public function update_password(Request $request)
	{
		try
		{
			if(!empty($request->id)&&!empty($request->old_password)&&!empty($request->newPassword)){
			$users = User::findOrFail($request->id);

				if(Hash::check($request->old_password, $users->encrypted_password))
				{
					   $users->fill([
						'encrypted_password' => Hash::make($request->newPassword)
						])->save();
							return response()->json(['status'=>200,'message'=>'success']);
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'old password not match']);
				}
			}
			else 
			{
				return response()->json(['status'=>400,'message'=>'param missing']);
			}
		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);	
		}
				 
	}
	
	public function logout(Request $request)
	{
		if(!empty($request->user_id)&&!empty($request->device_id)){
			
			try
			{
				$user_Session = UserSession::Where('user_id',$request->user_id)->Where('device_id',$request->device_id)->delete();
				if($user_Session)
				{	
				return response()->json(['status'=>200,'message'=>'success']);
				}
				else 
				{
				return response()->json(['status'=>500,'message'=>'error']);
				}
			}
			catch(Exception $e)
			{
			return response()->json(['status'=>500,'message'=>'error']);	
			}
				 
		}
	
		else 
		{
		return response()->json(['status'=>400,'message'=>'param missing']);
		}
	}

	public function delete(Request $request)
    {
		if(!empty($request->id)){
		$ids=explode(',',$request->id);
		keypads::destroy($ids);
		return response()->json(['status'=>204,'message'=>'success']);
		}
		else
		{
		return response()->json(['status'=>400,'message'=>'params missing']);
		}
    }
    	public function add_security(Request $request)
	{
		if(!empty($request->id)&&!empty($request->security_questions))
		{
			try
			{
				$users = User::findOrFail($request->id);
				if($users)
				{
					$users->security_questions=$request->security_questions;
					$save=$users->save();
					return response()->json(['status'=>200,'message'=>'success']);
				}
				else
				{
					return response()->json(['status'=>503,'message'=>'error']);
				}
			}
			catch(Exception $e)
			{
				return response()->json(['status'=>503,'message'=>'error']);
			}
		}
		else
		{
			return response()->json(['status'=>400,'message'=>'params missing']);
		}
		
	}

	public function checkpwd(Request $request)
	{
		try
		{
			if(!empty($request->id)&&!empty($request->password)){
			$users = User::findOrFail($request->id);

				if(Hash::check($request->password,$users->encrypted_password))
				{
					return response()->json(['status'=>200,'message'=>'success']);
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'password not match']);
				}
			}
			else 
			{
				return response()->json(['status'=>400,'message'=>'param missing']);
			}
		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);
		}
	}

		public function getsecurity(Request $request)
	{
		if(!empty($request->id)){
			
			try
			{
				$result = User::select('security_questions')->Where('id',$request->id)->first();
				if($result)
				{	
					$secure=json_decode($result->security_questions,true);
					return response()->json(['status'=>200,'message'=>'success','data'=>$secure]);
				}
				else 
				{
				return response()->json(['status'=>500,'message'=>'error']);
				}
			}
			catch(Exception $e)
			{
			return response()->json(['status'=>500,'message'=>'error']);	
			}
				 
		}
	
		else 
		{
		return response()->json(['status'=>400,'message'=>'param missing']);
		}
	}

	public function update_pwd(Request $request)
	{
		try
		{
			if(!empty($request->id)&&!empty($request->password)){
			$users = User::findOrFail($request->id);

				if($users)
				{
					$users->fill([
						'encrypted_password' => Hash::make($request->password)
						])->save();
					    //$users->encrypted_password=$request->password;
	  					//$save=$users->save();
	   					return response()->json(['status'=>200,'message'=>'success']);
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'password not update']);
				}
			}
			else 
			{
				return response()->json(['status'=>400,'message'=>'param missing']);
			}
		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);	
		}
				 
	}

	public function block_user(Request $request)
	{
		try
		{
			if(!empty($request->id)){
			$user = User::findOrFail($request->id);

				if($user)
				{
					$user->status=0;
					$user->save();
					    
	   				return response()->json(['status'=>200,'message'=>'success']);
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'user not found']);
				}
			}
			else 
			{
				return response()->json(['status'=>400,'message'=>'param missing']);
			}
		}
		catch(Exception $e)
		{
			return response()->json(['status'=>500,'message'=>'error']);	
		}
				 
	}

	public function userid_keypadid_delete(Request $request)
	{
		if(!empty($request->user_id)&&!empty($request->keypad_id)){		
			try
			{
				$result = Userkeypad::Where('user_id',$request->user_id)->where('keypad_id',$request->keypad_id)->delete();
				if($result)
				{	
					return response()->json(['status'=>200,'message'=>'success']);
				}
				else 
				{
				return response()->json(['status'=>500,'message'=>'error']);
				}
			}
			catch(Exception $e)
			{
			return response()->json(['status'=>500,'message'=>'error']);	
			}
				 
		}
	
		else 
		{
		return response()->json(['status'=>400,'message'=>'param missing']);
		}
	}

	public function add_user_keypad_details(Request $request)
	{
		if(!empty($request->keypad_id)&&!empty($request->phone_number))
		{
			try
			{	
				$user=User::where('phone_number',$request->phone_number)->first();
				if($user)
				{
					$door=Doors::where('id',$request->keypad_id)->count();
					if($door)
					{
						if(Userkeypad::where('keypad_id',$request->keypad_id)->where('user_id',$user->id)->where('status',1)->count())
						{
							return response()->json(['status'=>503,'message'=>'user already have access of door']);
						}
						$users=Userkeypad::where('keypad_id',$request->keypad_id)->where('user_id',$user->id)->count();
						if($users<1)
						{	
							$users=new Userkeypad(); 
							$users->user_id=$user->id;
							$users->keypad_id=$request->keypad_id;					
							$users->phone_number=$request->phone_number;
							$users->status=true;
							$users->user_name=$request->user_name;
							$save=$users->save();
							return response()->json(['status'=>200,'message'=>'success']);
						}
						else
						{
							return response()->json(['status'=>503,'message'=>'user already requested for door']);
						}
					}

					else
					{

						return response()->json(['status'=>400,'message'=>'door not found']);
					}
				}
				else
				{	
					$door=Doors::where('id',$request->keypad_id)->count();
					if($door)
					{
						if(Invites::where('keypad_id',$request->keypad_id)->where('mobile_no',$request->phone_number)->count())
						{
							return response()->json(['status'=>503,'message'=>'Already exist']);
						}
							$invite=new Invites();
							$invite->mobile_no=$request->phone_number;
							$invite->keypad_id=$request->keypad_id;
							$save=$invite->save();
							$message="A user is invited access the door";
							$this->send_sms($request->phone_number,$message);
							return response()->json(['status'=>200,'message'=>$message]);
					}
					else
					{
						return response()->json(['status'=>400,'message'=>'door not found']);
					}

									
				}

				
			}
			catch(Exception $e)
			{
				return response()->json(['status'=>503,'message'=>'error']);
			}
		}
		else
		{
			return response()->json(['status'=>400,'message'=>'params missing']);
		}
		
	}
	public function get_user_keypad_details(Request $request)
	{
		if(!empty($request->keypad_id))
		{
			try
			{	
				$user_id=[];
				$keypads=Userkeypad::where('keypad_id',$request->keypad_id)->get();

				if($keypads)
				{	
					$door=Doors::where('id',$request->keypad_id)->first();

					 	foreach($keypads as $keypad)
					 	{
					 		
							if($door->admin_id == $keypad->user_id)
							{

								$keypad->is_admin=1;
							}
							else
							{
								$keypad->is_admin=0;
							}
							
						}

					// foreach($keypads as $keypad)
					// {	
					// 	$door=Doors::where('id',$keypad->keypad_id)->first();
					// 	if($door->admin_id != $keypad->user_id)
					// 	{
					// 		if($keypad->status==1)
					// 		{
					// 			$verify=1;
					// 		}
					// 		else
					// 		{
					// 			$verify=0;
					// 		}
					// 		$user=User::where('id',$keypad->user_id)->first();
					// 		if($user)
					// 		{
					// 			$user->verify=$verify;
					// 		}
					// 		$users[]=$user;
					// 	}

					// }

					// $users=User::whereIn('id',$user_id)->get();

					// if(count($users)>0)
					// {
					return response()->json(['status'=>200,'message'=>'success','data'=>$keypads]);
					// }
					// else
					// {
					// 	return response()->json(['status'=>200,'message'=>'no users found']);
					// }

				}
				else
				{
					return response()->json(['status'=>503,'message'=>'error']);
				}
			}
			catch(Exception $e)
			{
				return response()->json(['status'=>503,'message'=>'error']);
			}
		}
		else
		{
			return response()->json(['status'=>400,'message'=>'params missing']);
		}
	}

	public function get_admin_details(Request $request)
	{
		if(!empty($request->id)){
			
			try
			{	

				$user=User::where('id',$request->id)->first();
				if($user)
				{
					if($user->status != 1)
					{
						return response()->json(['status'=>400,'message'=>'user not active']);
					}
					$result = Doors::Where('admin_id',$request->id)->get();
					if(count($result)>0)
					{
						return response()->json(['status'=>200,'message'=>'success','data'=>$result]);
					}
					else 
					{
						return response()->json(['status'=>500,'message'=>'no door found']);
					}
				}
				else
				{
					return response()->json(['status'=>400,'message'=>'user not found']);
				}
				
			}
			catch(Exception $e)
			{
			return response()->json(['status'=>500,'message'=>'error']);	
			}
				 
		}
	
		else 
		{
		return response()->json(['status'=>400,'message'=>'param missing']);
		}
	}


}

