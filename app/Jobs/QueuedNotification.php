<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Traits\SendNotification;

class QueuedNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SendNotification;

    public $user;
    public $content;
    public $url;

    public function __construct($user,$content,$url)
    {
        $this->user=$user;
        $this->content=$content;
        $this->url=$url;
    }

    public function handle()
    {
        $this->one_to_one($this->user,$this->content,$this->url);
    }
}
