<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    // use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','encrypted_password','reset_password_token','reset_password_sent_at','remember_created_at','sign_in_count','current_sign_in_at','last_sign_in_at','current_sign_in_ip','last_sign_in_ip','first_name','last_name','phone_number','verified','avatar','user_type','invited_at','invited_status','keypad_at','keypad_number','keypad_code','keypad_password','token','email','security_questions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
