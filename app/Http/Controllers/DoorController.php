<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Doors;
use App\Jobs\QueuedNotification;
use App\UserSession;

class DoorController extends Controller
{
    
	
	public function DoorList(Request $r)
	{
		
		
		if(!empty($r->user_id))
		{
			
				
				    	
						
						$doors = DB::table('user_keypads')
            ->join('keypads', 'keypads.id', '=', 'user_keypads.keypad_id')
            
            ->select('keypads.*')->where('user_keypads.user_id',$r->user_id)->where('user_keypads.status',true)
            ->get();
			
			return response()->json(['doors'=>$doors,'message'=>'success','status'=>201]);
			
			
			
						
					
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
		
		
		
		
	}
	
	
	
	public function PendingRequest(Request $r)
	{
		
		 $count=DB::table('notifications')->where('user_id',$r->user_id)->where('type',1)->count();
		 
		 	 if($count>0)
					return response()->json(['status'=>true]);
				else
					
					return response()->json(['status'=>false]);	
		 
		 
		
		
	}	
	
	public function CreateDoor(Request $r)
	{
		if(!empty($r->door_name)&&!empty($r->sim_no)&&!empty($r->code)&& !empty($r->phone_no)&&!empty($r->admin_code)&&!empty($r->email)&& !empty($r->admin_id))
		{
			try{
				
				    	
						
						
						
						 $data=DB::table('keypads')->where('sim_number',$r->sim_no)->first();
						 
						
				 
				 if(count($data)>0)
					return response()->json(['message'=>'sim_no already exists','status'=>400]);	
				
						
						
						
				    	$insert=new Doors;
						$insert->number=$r->phone_no;
						$insert->email=$r->email ;
						
						$insert->sim_number=$r->sim_no;
						$insert->admin_id=$r->admin_id;
						$insert->code=$r->code;
						$insert->admin_code=$r->code;
						$insert->door_name=$r->door_name;
						
						
			
						
						$insert->save();
						
						DB::table("user_keypads")->insert(['user_id'=>$r->admin_id,'keypad_id'=>$insert->id,'email'=>$r->email,'phone_number'=>$r->phone_no,"status"=>true]);
						
						return response()->json(['door_details'=>$insert,'message'=>'success','status'=>201]);
						
					}
				
				
				
			
			catch(Exception $e)
			{
				return response()->json(['message'=>'error','status'=>503]);
			}
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
	}
	
	
	
	public function AcceptDoorAccessRequest(Request $r)
	{
		
		
		if(!empty($r->access_id) && !empty($r->user_id) && !empty($r->status) && !empty($r->notification_id) && !empty($r->sim_no) )
		{
			try{
				
				    	 
                 
				 
							
							
							DB::table('user_keypads')
            ->where('id',$r->access_id)
            ->update(['status' => true]);
			
			
			
			if($r->status=="true")
			
			
			$type="4";
			else
			$type="5";
			
			
			
			 $data_array=DB::table('keypads')->where('sim_number',$r->sim_no)->first();
			
			
				DB::table('notifications')
            ->where('id',$r->notification_id)
            ->update(['type' =>$type]);
			
			
			  $data_array= json_encode($data_array, true);
			
			
							$data=DB::table('user_sessions')->select('token')->where('user_id',$r->user_id)->get();
			              if($r->status=="true")
							{
							 					 
							 DB::table("notifications")->insert(['user_id'=>$r->user_id,'type'=>2,'notification_data'=>$data_array]);
							 
							 
							  $url="";
						        $content = array(
						               "en" => "You Door Access Request has been Approved By Door Admin"
						               );
							}
							else
							{
									  $url="";
						        $content = array(
						               "en" => "You Door Access Request has been Rejected By Door Admin"
						               );
						         DB::table("notifications")->insert(['user_id'=>$r->user_id,'type'=>3,'notification_data'=>$data_array]);
							}
										   
			   
			   
        QueuedNotification::dispatch($data,$content,$url);
							
							
							return response()->json(['message'=>'Success','status'=>201]);	
						}
						
						
						
					
					
					
					
					
					
					
					
					
					
				
				
				
			
			catch(Exception $e)
			{
				return response()->json(['message'=>'error','status'=>503]);
			}
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
	
	
	}
	
	
	public function RequestDoorAccess(Request $r)
	{
		if(!empty($r->sim_no) && !empty($r->user_id) && !empty($r->phone_no) && !empty($r->email))
		{
			try{    	 
                 
				 $data=DB::table('keypads')->where('sim_number',$r->sim_no)->first();
				 
				 
				 
				 if(count($data)<=0)
					return response()->json(['message'=>'no door found','status'=>400]);	
				
				else
					
						
						{
							if($data->admin_id==$r->user_id)
							{
								return response()->json(['message'=>'You already have access of this door','status'=>400]);
							}

							
							$notification_data=DB::table('user_sessions')->select('token','id')->where('user_id',$data->admin_id)->get();
							
							 $data->user_phone_no=$r->phone_no;
				 			$data->user_id=$r->user_id;
				 			
				 			if(!DB::table('user_keypads')->where('user_id',$r->user_id)->where('keypad_id',$data->id)->count())
				 			{
				 				$id=DB::table("user_keypads")->insertGetId(['user_id'=>$r->user_id,'keypad_id'=>$data->id,'email'=>$r->email,'phone_number'=>$r->phone_no]);
				 				$data->access_id=$id;
							
						
								$data_array= json_encode($data, true);
							
								DB::table("notifications")->insert(['user_id'=>$data->admin_id,'type'=>1,'notification_data'=>$data_array]);
							
							  	$url="";
						        $content = array(
						               "en" => "You Received A New Door Access Request from ".$r->phone_no
						               );
									   
									   
									   
						        QueuedNotification::dispatch($notification_data,$content,$url);
							 
							 
								return response()->json(['message'=>'Your Request has been Sent To Door Admin','status'=>201]);	
				 			}
				 			else
				 			{
				 				return response()->json(['message'=>'You already requested for door','status'=>400]);
				 			}
							
						}
						
						
						
					}
					

			catch(Exception $e)
			{
				return response()->json(['message'=>'error','status'=>503]);
			}
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
	}
	
	
	
	
}
