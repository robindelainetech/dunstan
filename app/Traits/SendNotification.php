<?php 

namespace App\Traits;

trait SendNotification
{

    public function one_to_one($users,$content,$url)
    {
      $player_id=[];
	  
	  foreach($users as $user){
		  if($user->token!="" || $user->token!=NULL)
		  {
			$player_id[]=$user->token;
		  }
	  }
      

        
        // $player_id=implode(',',$player_id);

          return $this->send($player_id,$content,$url);
			

    }

    

    public function send($player_id,$content,$url)
    {
        $app_id=config('app.onesignal_app_id');
        $fields = array(

           'app_id' => $app_id,

           // 'included_segments' => $pp,

           'include_player_ids' => $player_id,

           

              'data' => array("key" => "value"),

           'contents' => $content,

           'url'=>$url,

          

          );

          $fields = json_encode($fields);

           

          $ch = curl_init();

          

            

          curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic MDQyY2E5MTEtMzdhZS00N2EwLWI4N2UtZTM3NGFlMjcxYWUw',

                                                     'Content-Type: application/json; charset=utf-8'));

          curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

          curl_setopt($ch, CURLOPT_HEADER, FALSE);

          curl_setopt($ch, CURLOPT_POST, TRUE);

          curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);



          $response = curl_exec($ch);

          curl_close($ch);

          $result=json_decode($response,true);

      // print_r($result);

      
          return $result;
    }



}