<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('check_connection','UserController@check_connection');
Route::post('signup','UserController@signup');
Route::post('login','UserController@login');
Route::post('CreateDoor','DoorController@CreateDoor');
Route::post('DoorList','DoorController@DoorList');
// Route::post('logout','UserController@logout');
Route::post('RequestDoorAccess','DoorController@RequestDoorAccess');
Route::post('AcceptDoorAccessRequest','DoorController@AcceptDoorAccessRequest');
Route::post('PendingRequest','DoorController@PendingRequest');
Route::post('/send_notification','NotificationController@send_notification');
Route::post('/NotificationsList','UserController@NotificationsList');

Route::post('/delete','API\UserController@delete');//for delete door
Route::post('/update_password','API\UserController@update_password');
Route::post('/logout','API\UserController@logout');
Route::post('send_sms','API\UserController@send_sms');
Route::get('send_sms/{mobile}','API\UserController@send_sms');
Route::post('add_security','API\UserController@add_security');
Route::post('checkpwd','API\UserController@checkpwd');
Route::post('getsecurity','API\UserController@getsecurity');
Route::post('update_pwd','API\UserController@update_pwd');
Route::post('userid_keypadid_delete','API\UserController@userid_keypadid_delete');
Route::post('add_user_keypad_details','API\UserController@add_user_keypad_details');
Route::post('get_user_keypad_details','API\UserController@get_user_keypad_details');
Route::post('get_admin_details','API\UserController@get_admin_details');

Route::post('door_lock_unlock','API\DoorController@lock_unlock');
Route::post('block_user','API\UserController@block_user');
Route::post('reciever.php','API\DoorController@reciever');