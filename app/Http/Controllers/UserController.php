<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Hash;
use Mail;
use App\Invites;
use App\Userkeypad;
class UserController extends Controller
{
    
	
	public function check_connection()
	{
		echo 'hello';
		$data=DB::table('answers')->count();
		echo $data; 
		
		$data1 = array('name'=>"Deepanshu Gandhi");
		
		 Mail::send('mail', $data1, function($message) {
         $message->to('deepanshu77777@gmail.com', 'Tutorials Point')->subject
            ('Dunstan Door Access Request');
         $message->from('developers.delainetech@gmail.com','Delaine Developers');
      });
		
		
	}
	
	
	public function login(Request $r)
	{
		
		if(!empty($r->phone_no)&&!empty($r->password) && !empty($r->device_id) && !empty($r->token))
		{
			
			try
			{
				$worker=User::where('phone_number',$r->phone_no)->first();
				
				if(count($worker)>0)
				{
					if($worker->status==0)
					{
						return response()->json(['message'=>' Your account is blocked','status'=>400]);
					}

					if(Hash::check($r->password, $worker->encrypted_password))
					{
						
						
						
						$exist = DB::table('user_sessions')->where(['device_id'=>$r->device_id])->first();
						
             if(count($exist)  >0) {
	
		DB::table('user_sessions')
            ->where('device_id',$r->device_id)			
            ->update(['token' =>$r->token,'user_id'=>$worker->id]);
   
         }
        else  {
 
 	      $insert=DB::table("user_sessions")->insert(['user_id'=>$worker->id,'device_id'=>$r->device_id,'token'=>$r->token]);

        }

					
						
						return response()->json(['user_details'=>$worker,'message'=>'success','status'=>201,'usertype'=>0]);
						
						
						
						
					}
					else
					{
						return response()->json(['message'=>'wrong password','status'=>400,'usertype'=>0]);
					}
				}
				
				
				else
				{
					return response()->json(['message'=>'mobile not exist','status'=>400]);
				}	
			}
			catch(Exception $e)
			{
				return response()->json(['message'=>'error','status'=>503,]);
			}
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
	}
	
	
	
	
	public function NotificationsList(Request $r)
	{
		
    if(!empty($r->user_id))
     {
		 
	 $results=DB::table('notifications')->where('user_id',$r->user_id)->get()->toArray();
	 
	
	 
	return response()->json(['notifications'=>$results,'message'=>'success','status'=>201]);

    	 
		 
	}
	
	else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
		
		
		
	}
	
	
	
	public function signup(Request $r)
	{
		if(!empty($r->email)&&!empty($r->phone_no)&&!empty($r->password) &&!empty($r->device_id)&&!empty($r->token))
		{
			try{


					$user_exists=User::where('phone_number',$r->phone_no)->count();
					if($user_exists>0)
					{
						return response()->json(['message'=>'phone_no already exist','status'=>400]);
					}
					else
					{
					    	$insert=new User;
							$insert->phone_number=$r->phone_no;
							$insert->email=$r->email;
							
							$insert->encrypted_password=Hash::make($r->password);											
							$save=$insert->save();
							
							if($save)
							{
								$invite=Invites::where('mobile_no',$r->phone_no)->get();
								if($invite)
								{
									foreach($invite as $inv)
									{
										$users=Userkeypad::where('keypad_id',$inv->keypad_id)->where('user_id',$insert->id)->count();
										if($users<1)
										{	
											$users=new Userkeypad(); 
											$users->user_id=$insert->id;
											$users->keypad_id=$inv->keypad_id;					
											$users->phone_number=$r->phone_no;
											$users->status=true;
											$users->email=$r->email;
											$save1=$users->save();
											if($save1)
											{
												Invites::where('mobile_no',$r->phone_no)->where('keypad_id',$inv->keypad_id)->delete();
											}
										}
									}
									

									
								}

								$exist = DB::table('user_sessions')->where(['device_id'=>$r->device_id])->first();
							
						             if(count($exist)  >0) {
							
								DB::table('user_sessions')
						            ->where('device_id',$r->device_id)			
						            ->update(['token' =>$r->token,'user_id'=>$insert->id]);
						   
						         }
						        else  {
						 
						 	      $insert1=DB::table("user_sessions")->insert(['user_id'=>$insert->id,'device_id'=>$r->device_id,'token'=>$r->token]);

						        }
								
							}
							
							
							return response()->json(['user_details'=>$insert,'message'=>'success','status'=>201]);
							
						}
				}
				
				
			
			catch(Exception $e)
			{
				return response()->json(['message'=>'error','status'=>503]);
			}
			
		}
		else
		{
			return response()->json(['message'=>'params missing','status'=>400]);
		}
	}
	
	
	
		 
}
