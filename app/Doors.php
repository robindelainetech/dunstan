<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doors extends Model
{
    protected $table="keypads";
	
	
	protected $fillable = [
        'door_name', 'email', 'sim_number','code','admin_code','admin_id','number'
    ];
}
