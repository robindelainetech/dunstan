<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\QueuedNotification;
use App\UserSession;

class NotificationController extends Controller
{
    public function send_notification(Request $r)
    {
        $user=UserSession::where('id','26')->first();
        $url="";
        $content = array(
               "en" => "Your recieved a notification."
               );
        QueuedNotification::dispatch($user,$content,$url);
    }
}
