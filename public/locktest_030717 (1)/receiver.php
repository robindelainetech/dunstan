<?php
/*
The lock POSTs any status messages in JSON format to this file "receiver.php".
This code:
* updates the local status
* sends a reply to the lock in json format. The lock acts on any lock/unlock request in the reply, then sends a new status message

An example POST from the lock would be:
{"keypad":"1234","password":"p*ssw*rd","locked":true,"error":"0"}

An example reply to the lock when it has POSTed would be:
{"keypad":"1234","token":"_V-KnG6h8wtY6PK7MTyOyoOvyyRWi2TI6qLFo9AZgjA","lock_request":false}
The lock decodes this JSON string, locks or unlocks the door according to the parameter "lock_request" then sends another POST to confirm its action.
*/

require 'aws-autoloader.php';
use Aws\Sns\SnsClient;

$status_file = '/tmp/current_status.log';		// TODO this would be a link to a database entry
$request_file = '/tmp/lock_request.txt';		// TODO this would be a link to a database entry

if ($_SERVER['REQUEST_METHOD'] === 'POST')
{

$token = "_V-KnG6h8wtY6PK7MTyOyoOvyyRWi2TI6qLFo9AZgjA"; // TODO a dummy token for now. it should be generated from the password sent by the lock.

$message = file_get_contents('php://input');
  
 if(!empty($message))
  {
  $password = "not set";
  $keypad = "no ID";
  $locked = "unknown";
  $bell = "no bell";
  $error = -1;
  $open = "unknown";
  
  $request = file_get_contents('php://input');        // read the POST data
  $input = json_decode($request, true);               // get an array object

  $password = $input['password'];                    	 // extract the password from the array
  $keypad = $input['keypad'];                          	 // extract the keypad ID from the array
  $locked = $input['locked'];				// true or false
  $bell = $input['bell'];					// true if bell has been pushed
  $error = $input['error'];					//-1 -> unknown,  0 -> no error, 1 -> invalid request, 2 -> invalid password, 3 -> not implemented yet, others TBD
  $open = $input['open'];					// 0 -> closed
 
// TODO validate these parameters, e.g. check that the password is correct and handle any errors.
// TODO create a new token from the received password. The lock will check that the decrypted token matches the password.
// NOTE the encryption method and public/private keys are TBD. Until then the lock will accept any token.

// Store the latest status
$current_status = json_encode
  (array
	(
	"keypad" => $keypad,
	"token" => $token,
	"locked" => $locked, 
	"bell" => $bell=$input,
	"error" => $error,
	"open" => $open
	)
  );    // convert to json
// delete and re-create the file
  file_put_contents($status_file, $current_status);		// TODO this should go to the database entry for this lock ID

// Get the current lock/unlock request TODO This should extract the required message from a database for this lock ID.
if(file_exists($request_file))
 {
 $lock_request = true;
 }
else
 {
 $lock_request = false;
 }


// Send a reply to the lock. It will act on any change of lock/unlock request, then send a new status update

  $reply_json = json_encode(array("keypad" => $keypad,"token" => $token,"lock_request" => $lock_request));    // convert to json
  echo ($reply_json);                                 // send the reply
  }	// end if(!empty)
  else
  {
  $reply_json = json_encode(array("keypad" => $keypad,"error" => 1));    // convert to json
  echo ($reply_json);                                 // send an error reply. DO NOT include the token in case it is malicious.
  }
}	// end "if POST"
else
{
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://ww3.org/TR/html4/loose.dtd">
<html>
<head>
<title></title>
</head>
<body>
Error.
</body>
</html>
<? 
} 
?>
