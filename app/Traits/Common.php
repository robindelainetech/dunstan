<?php 
namespace App\Traits;

use Auth;
use App\User;
use AWS;
use Aws\Sns\Exception\SnsException;
use Aws\Sns\Exception\NotFoundException;
use Aws\Sns\Exception\InvalidParameterException;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

trait Common
{

    public function send_sms($mobile,$message)
    {
      	try
      	{
      		$sms = AWS::createClient('sns');
	        $data=$sms->publish([
                'Message' => $message,
                'PhoneNumber' => $mobile,    
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType'  => [
                        'DataType'    => 'String',
                        'StringValue' => 'Transactional',
                     ]
                 ],
              ]);
	        $message_id=$data->get('MessageId');
	        $metadata=$data->get('@metadata');
	        $statusCode=$metadata['statusCode'];
	        if($statusCode==200)
	        {
				return "success";
	        }
	        else
	        {
	        	return "error";
	        }

		}
		catch(SnsException $e)
		{
			return "aws error";
		}
		catch(NotFoundException $e)
		{
			return  "not found";
		}
		catch(InvalidParameterException $e)
		{
			return  "invalid parameter";
		}
		catch(Exception $e)
		{
			return  "error";
		}
    }



}