<!DOCTYPE html>
<html lang="en">
	<head>
		<title>@yield('title','Dunstan Admin')</title>
		@include('admin.includes.meta')
		@include('admin.includes.css')
	</head>
	<body class="fix-header fix-sidebar card-no-border">
		  <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    	<div id="main-wrapper">
    		@include('admin.includes.header')
    		@include('admin.includes.leftsidebar')
    		@yield('content')

		</div>
		
		@include('admin.includes.js')
		@include('admin.includes.footer')
        </div>
    </div>
	</body>
</html>